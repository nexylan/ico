compose=docker-compose \
	--file docker-compose.dev.yml \
	--file docker-compose.yml

all: build up ps

build:
	$(compose) build

up:
	$(compose) up --detach app health

ps:
	$(compose) ps
	@echo App: http://localhost:`$(compose) port app 8080 | cut -d : -f 2`

clean:
	$(compose) down --remove-orphans --volumes

test:
	$(compose) run test
