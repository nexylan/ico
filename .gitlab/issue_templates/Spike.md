# Summary

## Problematic to solve

<!--
  Describe the problematic to solve onto one question (or more if really needed).
  Examples:
    - What is the best technology to setup a ...?
    - Is it possible to do ...?
    - How shall we be organized to do ...?

  MANDATORY: Add related issues with business value links (blocks).
  See also: https://docs.gitlab.com/ee/user/project/issues/related_issues.html

  Documentation about spikes (FR): https://blog.myagilepartner.fr/index.php/2017/03/20/spike-scrum/
-->

## Resources

<!--
  Add any sub section as needed.
  This list should be completed before spike start but can be completed during the spike work.
-->

### Tools

- ...

### Documentations

- ...

## Road map

<!--
  Describe what you are gonna do for this spike.
  A spike can be managed by (not exhaustive):
    - Looking for documentation and write its own.
    - Creating a POC (Proof of concept) merge request on the related project.
    - Creating a POC independent project for testing (on the nexylan/lab group).
  Prefer a timed steps list for a better estimation.
  Note: Spike ARE NOT estimated with weight but with allowed time to work on it.
-->

1. ... (1 day)
2. ... (3 days)

## Conclusion

<!--
  Resume here your conclusion on a few words

  This part MUST be completed at the end of the spike.
  It's preferable to have a team meeting to discuss and complete the conclusion together.
-->

### Do we have enough information to resolve our problematic?

<!--
  Yes/No answer with details if needed.
  If no, you need to details what is missing to complete the spike in order to allowing more time.
-->

### Is our problematic doable?

<!--
  Tell if fetched information allows us to make what we want.
  If not, the related business issue have to be reconsidered.

  Note: You CAN NOT answer this question if the first one is not answered by a YES.
-->

### Additional context

<!--
  If you have additional details to add to our conclusion.
  Note: Except the 2 first questions, the conclusion format is up to you.
-->

/label ~"type::spike 🧪"
